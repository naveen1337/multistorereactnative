import React from 'react';
import {View, Text, StyleSheet} from 'react-native';
import {Image} from 'react-native-elements';
import variables from '../../variables';

export default function OrderHistory(props) {
  if (props.varient === 'info') {
    return (
      <View style={styles.infocardcontainer}>
        <View style={styles.header}>
          <View style={styles.headerleft}>
            <Text style={styles.orderhint}>OID</Text>
            <Text style={styles.orderid}>{props.orderid}</Text>
          </View>
          <Text style={styles.date}>{props.orderdate}</Text>
        </View>

        <View style={styles.bottom}>
          <Text style={styles.price}>Rs {props.orderprice}</Text>

          {props.orderstatus ? (
            <View style={styles.bottomleft}>
              <Text style={styles.statustext}>Delivered</Text>
              <Image
                style={styles.statusicon}
                resizeMode={'contain'}
                source={require('../../assets/icons/success.png')}
              />
            </View>
          ) : (
            <View style={styles.bottomleft}>
              <Text style={styles.statustext}>Failed</Text>
              <Image
                style={styles.statusicon}
                resizeMode={'contain'}
                source={require('../../assets/icons/fail.png')}
              />
            </View>
          )}
        </View>
      </View>
    );
  }

  if (props.varient === 'tracking') {
    return (
      <View style={styles.infocardcontainer}>
        <View style={styles.header}>
          <View style={styles.headerleft}>
            <Text style={styles.orderhint}>OID</Text>
            <Text style={styles.orderid}>{props.orderid}</Text>
          </View>
          <Text style={styles.date}>{props.orderdate}</Text>
        </View>

        <View style={styles.bottom}>
          <View style={{justifyContent: 'center', alignItems: 'center'}}>
            {props.statuslevel >= 1 ? (
              <Image
                style={{width: 48, height: 48}}
                source={require('../../assets/icons/placed.png')}
              />
            ) : (
              <Text>Wait</Text>
            )}

            <Text
              style={{
                marginVertical: 5,
              }}>
              Placed
            </Text>
          </View>

          <View style={{justifyContent: 'center', alignItems: 'center'}}>
            {props.statuslevel >= 2 ? (
              <Image
                style={{width: 48, height: 48}}
                source={require('../../assets/icons/placed.png')}
              />
            ) : (
              <Image
                style={{width: 48, height: 48}}
                source={require('../../assets/icons/packed.png')}
              />
            )}

            <Text
              style={{
                marginVertical: 5,
                justifyContent: 'center',
                alignItems: 'center',
              }}>
              Packed
            </Text>
          </View>

          <View style={{justifyContent: 'center', alignItems: 'center'}}>
            {props.statuslevel >= 3 ? (
              <Image
                style={{width: 48, height: 48}}
                source={require('../../assets/icons/placed.png')}
              />
            ) : (
              <Image
                style={{width: 48, height: 48}}
                source={require('../../assets/icons/ontheway.png')}
              />
            )}

            <Text
              style={{
                marginVertical: 5,
                justifyContent: 'center',
                alignItems: 'center',
              }}>
              On the way
            </Text>
          </View>

          <View style={{justifyContent: 'center', alignItems: 'center'}}>
            {props.statuslevel >= 4 ? (
              <Image
                style={{width: 48, height: 48}}
                source={require('../../assets/icons/placed.png')}
              />
            ) : (
              <Image
                style={{width: 48, height: 48}}
                source={require('../../assets/icons/delivered.png')}
              />
            )}

            <Text
              style={{
                marginVertical: 5,
                justifyContent: 'center',
                alignItems: 'center',
              }}>
              Delivered
            </Text>
          </View>
        </View>
      </View>
    );
  } else {
    return <Text>Hello</Text>;
  }
}

const styles = StyleSheet.create({
  infocardcontainer: {
    backgroundColor: '#FFFFFF',
    marginHorizontal: 20,
    padding: 10,

    borderRadius: 16,
    marginBottom: 10,
    flex: 1,
  },
  headerleft: {
    flexDirection: 'row',
  },
  header: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    marginBottom: 10,
  },
  orderhint: {
    color: variables.colors.brandcolor,
  },
  orderid: {
    textTransform: 'uppercase',
    fontFamily: variables.fonts.mediumfont,
    fontSize: 14,
    paddingHorizontal: 10,
  },
  date: {
    alignItems: 'flex-end',
    justifyContent: 'flex-end',
    fontFamily: variables.fonts.mediumfont,
    fontSize: 14,
  },
  bottom: {
    flexDirection: 'row',
    justifyContent: 'space-between',
  },
  bottomleft: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
  },
  price: {
    fontFamily: variables.fonts.semiboldfont,
    fontSize: 18,
    color: '#000000',
  },
  statustext: {
    fontSize: 14,
    fontFamily: variables.fonts.mediumfont,
    paddingHorizontal: 10,
  },
  statusicon: {width: 15, height: 15, marginBottom: 5},
});
