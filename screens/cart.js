import React from 'react';
import {View, Text, StyleSheet} from 'react-native';
import {ScrollView} from 'react-native-gesture-handler';
import {
  TopBar,
  CouponBox,
  ProductCard,
  OrderHistory,
  BrandCard,
  NearByBrand,
} from '../components/';
import variables from '../variables';

export default function Cart() {
  return (
    <ScrollView style={styles.root}>
      <TopBar varient="screen" title="Available Coupon Offers" />
      <View>
        <CouponBox
          title="First Order "
          info="10% offer on orders above 100. Maximum Rs. 50 offer"
        />
        <CouponBox
          title="Weekly Sale "
          info="10% offer on orders above 100. Maximum Rs. 50 offer"
        />
      </View>
      <View>
        <ProductCard
          productname="New Product"
          category="Soft Drinks"
          variation={[
            {id: 1, value: '1 pcs', price: 100, demoprice: 120},
            {id: 2, value: '2 pcs', price: 200, demoprice: 220},
            {id: 3, value: '3 pcs', price: 300, demoprice: 320},
          ]}
          offertext="Offer 10%"
        />
      </View>
      <View>
        <OrderHistory
          varient="info"
          orderid="TB200805001"
          orderdate="01-13-1950"
          orderprice="100"
          orderstatus={true}
        />
        <OrderHistory
          varient="info"
          orderid="TB200805001"
          orderdate="01-13-1950"
          orderprice="100"
          orderstatus={false}
        />

        <OrderHistory
          varient="tracking"
          orderid="TB200805001"
          orderdate="01-13-1950"
          orderprice="100"
          statuslevel={1}
        />
      </View>

      <View style={{paddingHorizontal: 25}}>
        <BrandCard
          brandname="The Example"
          category="Soft Drinks"
          rating="5"
          location="Pudukkottai"
          distance="2 kms"
        />
      </View>

      <View style={{paddingTop: 500}}></View>
    </ScrollView>
  );
}

const styles = StyleSheet.create({
  root: {
    height: '100%',
    backgroundColor: variables.colors.brandbackground,
  },
});
