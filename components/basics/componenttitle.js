import React from 'react';
import {View, Text, StyleSheet} from 'react-native';
import variables from '../../variables';
import Icon from 'react-native-vector-icons/Ionicons';

export default function ComponentTitle(props) {
  if (props.varient === 'withIconAction') {
    return (
      <View
        style={{
          flexDirection: 'row',
          justifyContent: 'space-between',
          marginVertical: 16,
          marginHorizontal:16
        }}>
        <View style={{flexDirection: 'row', justifyContent: 'space-between'}}>
          <Icon
            style={{paddingRight: 10}}
            name={props.iconname}
            size={20}
            color={variables.colors.brandcolor}
          />
          <Text style={styles.texttitle}>{props.title}</Text>
        </View>
        <View>
          <Text style={styles.subtitle}>{props.action}</Text>
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
 
  texttitle: {
    fontFamily: variables.fonts.mediumfont,
    fontSize: variables.fonts.regular,
  },
  subtitle: {
    fontFamily: variables.fonts.mediumfont,
    fontSize: variables.fonts.small,
    color: variables.colors.brandcolor,
    textDecorationLine: 'underline',
  },
});
