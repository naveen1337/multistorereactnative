import React from 'react';
import {View, Text, StyleSheet} from 'react-native';
import {Image} from 'react-native-elements';
import LinearGradient from 'react-native-linear-gradient';
import variables from '../../variables';
import Icon from 'react-native-vector-icons/Ionicons';

const TopSellerBrand = (props) => {
  return (
    <LinearGradient colors={['#2E2E2E', '#000000']} style={styles.rootbrand}>
      <View style={styles.topsellercontainer}>
        <Image
          style={styles.brandlogo}
          resizeMode={'contain'}
          source={require('../../assets/brand/5.png')}
        />
        <Text numberOfLines={2} style={styles.brandtext}>
          {props.brandname}
        </Text>
        <View style={styles.rating}>
          <Icon
            name="star"
            size={12}
            color={variables.colors.brandcolor}
          />
          <Text style={styles.ratingnumber}>5</Text>
        </View>
      </View>
    </LinearGradient>
  );
};
const styles = StyleSheet.create({
  rootbrand: {
    marginHorizontal: 5,
    paddingVertical: 10,
    paddingHorizontal: 3,
    borderRadius: 14,
  },
  topsellercontainer: {
    width: 112,
    paddingHorizontal: 7,
  },
  brandlogo: {
    width: '100%',
    height: 100,
  },
  brandtext: {
    fontFamily: variables.fonts.mediumfont,
    color: '#ffffff',
    paddingVertical: 5,
    width: '82%',
    height: 55,
    color: variables.colors.brandcolor,
    fontSize: 14,
  },
  rating: {
    flexDirection: 'row',
    alignItems: 'center',
  },
  ratingnumber: {
    color: '#8A8A8A',
    paddingHorizontal: 7,
    fontSize: 12,
  },
});
export default TopSellerBrand;
