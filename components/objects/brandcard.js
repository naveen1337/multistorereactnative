import React from 'react';
import {View, Text, StyleSheet, Image} from 'react-native';
import Icon from 'react-native-vector-icons/Ionicons';
import variables from '../../variables';

export default function BrandCard(props) {
  return (
    <View style={styles.container}>
      <Image
        style={styles.image}
        source={require('../../assets/brand/5.png')}
      />
      <View style={styles.textwrapper}>
        <View>
          <Text style={styles.brandname}>{props.brandname}</Text>
          <Text style={styles.category}>{props.category}</Text>
        </View>

        <View style={styles.bottom}>
          <View style={styles.rating}>
            <Icon name="star" size={12} color={variables.colors.brandcolor} />
            <Text style={styles.ratingnumber}>{props.rating}</Text>
          </View>

          <Text style={styles.bottomtext}>{props.location}</Text>
          <Text style={styles.bottomtext}>{props.distance}</Text>
        </View>
      </View>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flexDirection: 'row',
    flex: 1,
    borderRadius: 16, 
  },
  image: {
    width: 100,
    height: 100,
  },
  textwrapper: {
    flex: 1,
    justifyContent: 'space-between',
    paddingHorizontal: 10,
    paddingVertical: 5,
  },
  brandname: {
    fontFamily: variables.fonts.mediumfont,
    fontSize: 16,
    color: variables.colors.brandcolor,
  },
  category: {
    fontSize: 10,
    color: '#828282',
    fontFamily: variables.fonts.regularfont,
  },
  bottom: {
    flexDirection: 'row',
    justifyContent: 'space-between',
  },
  rating: {
    flexDirection: 'row',
    alignItems: 'center',
  },
  ratingnumber: {
    color: variables.colors.brandcolor,
    paddingHorizontal: 5,
  },
  bottomtext: {
    color: variables.colors.brandcolor,
    fontSize: 12,
    fontFamily: variables.fonts.regularfont,
  },
});
