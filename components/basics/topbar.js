import React from 'react';
import {View, StyleSheet, Image, Text, Pressable} from 'react-native';
import variables from '../../variables';

const TopBar = (props) => {
  if (props.varient === 'home') {
    return (
      <View style={styles.topbarcontainer}>
        <View style={styles.rightsection}>
          <Pressable onPress={() => props.navigation.openDrawer()}>
            <Image
              style={styles.navdrawericon}
              source={require('../../assets/icons/menu.png')}
            />
          </Pressable>
          <Image
            style={styles.brandlogo}
            resizeMode={'contain'}
            source={require('../../assets/brand/mainbrand.png')}
          />
        </View>

        <View style={styles.leftsection}>
          <View>
            <Image
              style={styles.letfsideicons}
              resizeMode={'contain'}
              source={require('../../assets/icons/user.png')}
            />
          </View>
          <View>
            <Image
              style={styles.letfsideicons}
              resizeMode={'contain'}
              source={require('../../assets/icons/cart.png')}
            />
          </View>
        </View>
      </View>
    );
  }

  if (props.varient === 'screen') {
    return (
      <View style={styles.topbarcontainer}>
        <View style={styles.rightsection}>
          <Image
            style={styles.backicon}
            resizeMode={'contain'}
            source={require('../../assets/icons/back.png')}
          />
        </View>

        <View style={styles.leftsection}>
          <View>
            <Text style={styles.leftsidetext}>{props.title}</Text>
          </View>
        </View>
      </View>
    );
  }
};

const styles = StyleSheet.create({
  // Home Screen Design
  topbarcontainer: {
    backgroundColor: variables.colors.Primary,
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    padding: 10,
  },
  rightsection: {
    flexDirection: 'row',
    justifyContent: 'space-around',
    alignItems: 'center',
  },
  navdrawericon: {
    width: 24,
    height: 24,
    marginTop: 10,
    marginHorizontal: 20,
  },
  leftsection: {
    flexDirection: 'row',
    justifyContent: 'space-around',
  },
  brandlogo: {
    width: 106,
  },
  letfsideicons: {
    width: 24,
    height: 24,
    marginHorizontal: 10,
  },
  // Screen Design
  backicon: {
    width: 24,
    height: 24,
  },
  leftsidetext: {
    paddingVertical: 5,
    paddingHorizontal: 5,
    letterSpacing: 1,
    color: variables.colors.normaltext,
    fontFamily: variables.fonts.medium,
  },
});
export default TopBar;
