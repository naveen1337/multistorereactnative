import React from 'react';
import {View, Text, StyleSheet} from 'react-native';
import variables from '../../variables';
export default function Contentbox(props) {
  if (props.varient === 'usernamecard') {
    return (
      <View style={userstyle.usercontainer}>
        <View>
          <Text style={userstyle.username}>{props.name}</Text>
          <Text style={userstyle.usercontact}>{props.email}</Text>
          <Text style={userstyle.usercontact}>{props.phone}</Text>
        </View>
        <View>
          <Text style={userstyle.edit}>EDIT</Text>
        </View>
      </View>
    );
  }
  if (props.varient === 'walletcard') {
    return (
      <View style={walletstyle.walletcontainer}>
        <View style={walletstyle.cardtitlecontainer}>
          <Text style={walletstyle.cardtitle}>Your Wallet</Text>
        </View>
        <View style={walletstyle.amountcontainer}>
          <Text style={walletstyle.symbol}>Rs</Text>
          <Text style={walletstyle.amount}>{props.amount}</Text>
        </View>
        <View style={walletstyle.walletwrapper}>
          <Text style={walletstyle.walletaction}>ADD MONEY</Text>
        </View>
      </View>
    );
  }
  if (props.varient === 'infocard') {
    return (
      <View style={infostyle.infocardcontainer}>
        <Text style={infostyle.title}>{props.title}</Text>
        <Text style={infostyle.gobutton}>></Text>
      </View>
    );
  }
  if (props.varient === 'addresscard') {
    return (
      <View style={addressstyle.infocardcontainer}>
        <View style={addressstyle.header}>
          <Text style={addressstyle.headertitle}>{props.title}</Text>
          <View style={addressstyle.actionbox}>
            <Text style={addressstyle.actiontext}>EDIT</Text>
            <Text style={addressstyle.actiontext}>DELETE</Text>
          </View>
        </View>
        <View>
          {props.address.map((item) => {
            return (
              <Text key={item} style={addressstyle.text}>
                {item}
              </Text>
            );
          })}
        </View>
      </View>
    );
  } else {
    return <Text>Not Found</Text>;
  }
}

const userstyle = StyleSheet.create({
  usercontainer: {
    flex: 1,
    justifyContent: 'space-between',
    flexDirection: 'row',
    backgroundColor: '#000000',
    borderBottomRightRadius: 16,
    borderBottomLeftRadius: 16,
    paddingHorizontal: 26,
    paddingVertical: 28,
  },
  username: {
    fontFamily: variables.fonts.regularfont,
    color: '#ffffff',
    fontSize: variables.fonts.large,
  },
  usercontact: {
    color: '#828282',
    fontFamily: variables.fonts.mediumfont,
    fontSize: variables.fonts.regular,
  },
  edit: {
    color: variables.colors.brandcolor,
    fontSize: variables.fonts.regular,
    fontFamily: variables.fonts.semiboldfont,
  },
});

const walletstyle = StyleSheet.create({
  walletcontainer: {
    flex: 1,
    marginVertical: 22,
    backgroundColor: '#FFFFFF',
    paddingHorizontal: 12,
    marginHorizontal: 10,
    padding: 10,
    flexDirection: 'row',
    flexWrap: 'wrap',
    justifyContent: 'space-between',
    borderRadius: 16,
  },
  cardtitlecontainer: {
    width: '100%',
  },
  cardtitle: {
    fontFamily: variables.fonts.semiboldfont,
    fontSize: variables.fonts.regular,
    color: '#828282',
  },
  amountcontainer: {
    flexDirection: 'row',
    alignContent: 'center',
    alignItems: 'center',
  },
  symbol: {
    color: '#828282',
    fontFamily: variables.fonts.mediumfont,
    fontSize: variables.fonts.regular,
  },
  amount: {
    fontSize: 30,
    fontFamily: variables.fonts.semiboldfont,
    marginHorizontal: 10,
  },
  walletwrapper: {
    marginTop: 15,
  },
  walletaction: {
    color: variables.colors.brandcolor,
    fontFamily: variables.fonts.semiboldfont,
  },
});

const infostyle = StyleSheet.create({
  infocardcontainer: {
    backgroundColor: '#FFFFFF',
    marginHorizontal: 10,
    padding: 10,
    flexDirection: 'row',
    borderRadius: 16,
    justifyContent: 'space-between',
    marginBottom: 10,
    alignItems: 'center',
    flex: 1,
  },
  title: {
    fontFamily: variables.fonts.mediumfont,
    fontSize: variables.fonts.regular,
  },
  gobutton: {
    fontFamily: variables.fonts.semiboldfont,
    fontSize: 20,
    color: variables.colors.brandcolor,
  },
});
const addressstyle = StyleSheet.create({
  infocardcontainer: {
    flex: 1,
    backgroundColor: '#FFFFFF',
    paddingHorizontal: 12,
    marginHorizontal: 10,
    padding: 10,
    borderRadius: 16,
    marginBottom: 10,
  },
  header: {
    width: '100%',
    flexDirection: 'row',
    justifyContent: 'space-between',
    marginBottom: 10,
  },
  headertitle: {
    fontFamily: variables.fonts.semiboldfont,
    fontSize: variables.fonts.regular,
  },
  actionbox: {
    flexDirection: 'row',
    justifyContent: 'space-around',
  },
  actiontext: {
    marginHorizontal: 10,
    fontFamily: variables.fonts.semiboldfont,
    fontSize: variables.fonts.regular,
    color: variables.colors.brandcolor,
  },
  text: {
    fontFamily: variables.fonts.regularfont,
    fontSize: variables.fonts.regular,
    lineHeight: 20,
  },
});
