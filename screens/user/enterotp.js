import React, {useState, useEffect} from 'react';
import {View, Text, StyleSheet} from 'react-native';
import {Button} from 'react-native-elements';
import {TopBar} from '../../components/';
import variables from '../../variables';
import auth from '@react-native-firebase/auth';

import {
  CodeField,
  Cursor,
  useBlurOnFulfill,
  useClearByFocusCell,
} from 'react-native-confirmation-code-field';

export default function enterotp() {
  const [fsotp, setfsotp] = useState([]);

  useEffect(() => {
    console.log('VERIFICATION INIT');
    auth()
      .signInWithPhoneNumber('+918778615921')
      .then((data) => {
        console.log(data);
        setfsotp(data);
      })
      .catch((err) => console.log(err));
  }, []);

  function initlogin(e) {
    console.log('PHONE VERIFICATION');
    fsotp
      .confirm('123457')
      .then((success) => console.log(success))
      .catch((err) => console.log(err));
  }

  const CELL_COUNT = 6;
  const [value, setValue] = useState('');
  const ref = useBlurOnFulfill({value, cellCount: CELL_COUNT});
  const [props, getCellOnLayoutHandler] = useClearByFocusCell({
    value,
    setValue,
  });
  return (
    <View style={styles.rootbox}>
      <TopBar varient="screen" title="Verify Mobile Number" />
      <View style={styles.textcontainer}>
        <Text style={styles.text}>Enter OTP </Text>
        <Text style={styles.text}>To Continue</Text>
      </View>

      <View style={styles.inputcontainer}>
        <CodeField
          ref={ref}
          {...props}
          value={value}
          onChangeText={setValue}
          cellCount={CELL_COUNT}
          rootStyle={styles.codeFieldRoot}
          keyboardType="number-pad"
          textContentType="oneTimeCode"
          renderCell={({index, symbol, isFocused}) => (
            <Text
              key={index}
              style={[styles.cell, isFocused && styles.focusCell]}
              onLayout={getCellOnLayoutHandler(index)}>
              {symbol || (isFocused ? <Cursor /> : null)}
            </Text>
          )}
        />
      </View>
      <View>
        <Text style={styles.resendtext}>Resend OTP</Text>
      </View>
      <View style={styles.otpbtn}>
        <Button
          type="clear"
          containerStyle={{
            backgroundColor: variables.colors.Primary,
            borderRadius: 8,
            paddingVertical: 8,
            marginHorizontal: 20,
            marginBottom: 20,
          }}
          titleStyle={{
            color: variables.colors.brandcolor,
            fontFamily: variables.fonts.normal,
          }}
          onPress={initlogin}
          title="CONTINUE"
        />
      </View>
    </View>
  );
}

const styles = StyleSheet.create({
  rootbox: {
    backgroundColor: variables.colors.brandbackground,
    flex: 1,
  },
  textcontainer: {
    marginTop: 80,
  },
  text: {
    textAlign: 'center',
    fontSize: variables.fonts.large,
    fontFamily: variables.fonts.semibold,
  },
  inputcontainer: {
    marginTop: 100,
    flexDirection: 'row',
    justifyContent: 'center',
  },
  title: {textAlign: 'center', fontSize: 30},
  codeFieldRoot: {marginHorizontal: 50},
  cell: {
    width: 25,
    height: 40,
    marginHorizontal: 10,
    lineHeight: 38,
    fontSize: 24,
    borderBottomColor: '#00000030',
    borderWidth: 2,
    borderColor: '#00000000',
    textAlign: 'center',
  },
  focusCell: {
    borderColor: '#00000000',
  },
  resendtext: {
    textAlign: 'center',
    marginVertical: 26,
    color: variables.colors.brandcolor,
    textDecorationLine: 'underline',
  },
  otpbtn: {
    position: 'absolute',
    bottom: 0,
    width: '100%',
  },
});
