import React from 'react';
import {createStackNavigator} from '@react-navigation/stack';

const Stack = createStackNavigator();

import Login from '../screens/user/login';
import Enterotp from '../screens/user/enterotp';
import Profile from '../screens/user/profile';

export default function Stacknavigator() {
  return (
    <Stack.Navigator screenOptions={{headerShown: false}}>
      <Stack.Screen name="login" component={Login} />
      <Stack.Screen name="profile" component={Profile} />
      <Stack.Screen name="enterotp" component={Enterotp} />
    </Stack.Navigator>
  );
}
