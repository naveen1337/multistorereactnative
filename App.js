import React from 'react';
import {StatusBar} from 'react-native';
import {NavigationContainer} from '@react-navigation/native';

import DrawerNavigator from './navigators/rootdrawer';
import store from './store';
import {Provider} from 'react-redux';

const App = () => {
  return (
    <>
      <Provider store={store}>
        <NavigationContainer>
          <StatusBar barStyle="dark-content" backgroundColor="#000000" />
          <DrawerNavigator />
        </NavigationContainer>
      </Provider>
    </>
  );
};

export default App;
