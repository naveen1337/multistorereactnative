import React from 'react';
import {View, Text, StyleSheet, Pressable} from 'react-native';
import {Image} from 'react-native-elements';
import variables from '../../variables';
import Icon from 'react-native-vector-icons/Ionicons';
import {Picker} from '@react-native-picker/picker';

export default function ProductCard(props) {
  const [quantity, setquantity] = React.useState(3);
  const [selected, setselected] = React.useState(props.variation[0]);

  function changevariation(e) {
    props.variation.find((item) => {
      if (item.value === e) {
        console.log(item);
        setselected(item);
        return 1;
      } else {
        return 0;
      }
    });
  }

  return (
    <View style={styles.root}>
      <View>
        <Image
          resizeMode={'contain'}
          style={styles.image}
          source={require('../../assets/brand/5.png')}
        />
      </View>
      <View style={styles.textwrapper}>
        <Text style={styles.productname}>{props.productname}</Text>
        <Text style={styles.category}>{props.category}</Text>
        <View
          style={{
            borderStyle: 'solid',
            borderWidth: 1,
            borderColor: '#000000',
            borderRadius: 5,
            width: 120,
            marginBottom: 10,
          }}>
          <Picker
            selectedValue={selected.value}
            mode="dropdown"
            style={{height: 40, width: '100%', flex: 1, borderColor: '#000000'}}
            onValueChange={(itemValue) => {
              changevariation(itemValue);
            }}>
            {props.variation.map((item) => {
              return (
                <Picker.Item
                  key={item.id}
                  label={item.value}
                  value={item.value}
                />
              );
            })}
          </Picker>
        </View>
        <View style={styles.bottomwrapper}>
          <View style={{flexDirection: 'row', alignItems: 'center'}}>
            <Text style={styles.price}>₹ {selected.price}</Text>
            <Text style={styles.demoprice}>₹ {selected.demoprice}</Text>
          </View>

          <View style={styles.action}>
            {quantity > 1 ? (
              <>
                <Pressable onPress={() => setquantity(quantity - 1)}>
                  <Icon name="remove-circle" size={35} color="#000000" />
                </Pressable>
                <Text style={styles.quantity}>{quantity}</Text>
              </>
            ) : (
              <Text></Text>
            )}

            <Pressable onPress={() => setquantity(quantity + 1)}>
              <Icon name="add-circle" size={35} color="#000000" />
            </Pressable>
          </View>
        </View>
        <Text style={styles.offertext}>{props.offertext}</Text>
      </View>
    </View>
  );
}

const styles = StyleSheet.create({
  root: {
    backgroundColor: '#ffffff',
    marginHorizontal: 20,
    paddingVertical: 5,
    paddingHorizontal: 5,
    flexDirection: 'row',
    marginVertical: 10,
    alignItems: 'center',
    flex: 1,
  },
  image: {
    width: 100,
    height: 100,
  },
  textwrapper: {
    paddingHorizontal: 10,
    flex: 1,
  },
  bottomwrapper: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
  },
  productname: {
    fontFamily: variables.fonts.regularfont,
    fontSize: 16,
  },
  category: {
    fontFamily: variables.fonts.regularfont,
    color: variables.colors.brandcolor,
    fontSize: 12,
  },
  price: {
    fontFamily: variables.fonts.semiboldfont,
    fontSize: 16,
  },
  demoprice: {
    paddingHorizontal: 7,
    fontSize: 12,
    alignItems: 'center',
    textDecorationLine: 'line-through',
  },
  action: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-around',
  },
  quantity: {
    marginHorizontal: 10,
    fontFamily: variables.fonts.semiboldfont,
    fontSize: 16,
  },
  offertext: {
    fontSize: 12,
    fontFamily: variables.fonts.regularfont,
    textTransform: 'uppercase',
    color: variables.colors.brandcolor,
  },
});
