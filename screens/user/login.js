import React, {useState} from 'react';
import {View, Text, StyleSheet, ScrollView} from 'react-native';
import {Image, Input, Button} from 'react-native-elements';
import LinearGradient from 'react-native-linear-gradient';
import variables from '../../variables';
import {useDispatch} from 'react-redux';
import messaging from '@react-native-firebase/messaging';
import {Dimensions} from 'react-native';

import {adddata} from '../../store/state/otp';

export default function Login() {
  return (
    <ScrollView>
      <LinearGradient
        colors={['#FFF0CE', '#E9CB96']}
        style={styles.wrapper}>
        <View style={{alignItems: 'center'}}>
          <Image
            style={styles.image}
            resizeMode={'contain'}
            source={require('../../assets/signin.png')}
          />
        </View>
      </LinearGradient>
      <View
        style={styles.loginbox}>
        <View
          style={styles.loginwrapper}>
          <Text
            style={styles.logintext}>
            Sign in
          </Text>
          <View>
            <Input
              style={styles.input}
              keyboardType="phone-pad"
              placeholder="Mobile No"
            />
          </View>

          <View>
            <Button
              type="clear"
              loadingProps={{
                size: 'small',
                color: variables.colors.brandcolor,
              }}
              containerStyle={{
                backgroundColor: '#000000',
                borderRadius: 8,
                paddingVertical: 8,
              }}
              titleStyle={{
                color: variables.colors.brandcolor,
              }}
              title="LOGIN  "
            />
          </View>
        </View>
      </View>
    </ScrollView>
  );
}

const styles = StyleSheet.create({
    wrapper:{
        height: Dimensions.get('window').height / 2 + 30,
        justifyContent: 'flex-start',
        paddingTop:50
      },
      image:{
        width: 233,
        height: 218,
      },
      loginbox:{
        backgroundColor: variables.colors.brandbackground,

      },
      loginwrapper:{
        backgroundColor: '#ffffff',
        marginHorizontal: 25,
        borderRadius: 16,
        paddingHorizontal: 34,
        paddingTop: 29,
        paddingBottom: 33,
        position: 'relative',
        bottom: 60,
      },
      logintext:{
        fontSize: 20,
        fontFamily: variables.fonts.semiboldfont,
        paddingLeft: 13,
      },
      input:{
        fontFamily: variables.fonts.regularfont,
        fontSize: 14,
        color: '#828282',
      },




});
