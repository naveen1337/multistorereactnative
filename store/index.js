import {configureStore} from '@reduxjs/toolkit';

import counteslice from './state/counter';
import otp from './state/otp';

const store = configureStore({
  reducer: {
    counter: counteslice,
    otp: otp,
  },
});

export default store;
