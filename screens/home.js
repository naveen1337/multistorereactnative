import React from 'react';
import {View, StyleSheet, Text, Pressable, Dimensions} from 'react-native';
import {ScrollView} from 'react-native-gesture-handler';
import variables from '../variables';

import {TopBar, TopSeller, ComponentTitle, NearByBrand} from '../components/';

const Home = (props) => {
  return (
    <ScrollView style={styles.root}>
      <TopBar
        varient="home"
        title="Home Screen No title"
        navigation={props.navigation}
      />

      <View>
        <ComponentTitle
          varient="withIconAction"
          iconname="arrow-up-circle"
          title="Top Sellers"
          action="View all"
        />

        <ScrollView horizontal={true} style={{flexDirection: 'row'}}>
          {[1, 2, 3, 4, 5, 6, 7].map((item) => {
            return (
              <View key={item}>
                <TopSeller brandname="The Bowl Company" />
              </View>
            );
          })}
        </ScrollView>
        <ComponentTitle
          varient="withIconAction"
          iconname="location"
          title="Nearby Restaurent"
          action="View all"
        />
      </View>
      <ScrollView horizontal={true}>
        <View>
            <NearByBrand
              brandname="Natural Icecreams "
              category="Chicken"
              location="Bangalore"
              distance="2kms"
            />
          
            <NearByBrand
              brandname="KFC"
              category="Chicken"
              location="Bangalore"
              distance="2kms"
            />
        </View>
        <View>
            <NearByBrand
              brandname="Natural Icecreams ,Natural IcecreamsNatural Ic Natural Icecreams ,Natural IcecreamsNatural Ic"
              category="Chicken"
              location="Bangalore"
              distance="2kms"
            />
          
            <NearByBrand
              brandname="KFC"
              category="Chicken"
              location="Bangalore"
              distance="2kms"
            />
        </View>
        <View>
            <NearByBrand
              brandname="Natural Icecreams "
              category="Chicken"
              location="Bangalore"
              distance="2kms"
            />
          
            <NearByBrand
              brandname="KFC"
              category="Chicken"
              location="Bangalore"
              distance="2kms"
            />
        </View>

      </ScrollView>
    </ScrollView>
  );
};

const styles = StyleSheet.create({
  root: {
    backgroundColor: variables.colors.brandbackground,
    height: '100%',
  },
});

export default Home;
