import React from 'react';
import {createBottomTabNavigator} from '@react-navigation/bottom-tabs';
import {View, Text, Image, TouchableOpacity, Pressable} from 'react-native';
import LinearGradient from 'react-native-linear-gradient';
import variables from '../variables';

import Home from '../screens/home';
import Search from '../screens/search';
import Cart from '../screens/cart';
import StackNavigator from './stacknavigator';

const Tab = createBottomTabNavigator();

function MyTabBar({state, descriptors, navigation}) {
  const image1 = '../assets/icons/home.png';
  const image2 = '../assets/icons/user.png';
  const image3 = '../assets/icons/cart.png';
  const image4 = '../assets/icons/user.png';

  const focusedOptions = descriptors[state.routes[state.index].key].options;

  if (focusedOptions.tabBarVisible === false) {
    return null;
  }

  return (
    <LinearGradient
      colors={['#2E2E2E', '#000000']}
      style={{borderTopRightRadius: 16, borderTopLeftRadius: 16}}>
      <View
        style={{
          flexDirection: 'row',
        }}>
        {state.routes.map((route, index) => {
          const {options} = descriptors[route.key];
          const label =
            options.tabBarLabel !== undefined
              ? options.tabBarLabel
              : options.title !== undefined
              ? options.title
              : route.name;

          const isFocused = state.index === index;

          const onPress = () => {
            const event = navigation.emit({
              type: 'tabPress',
              target: route.key,
              canPreventDefault: true,
            });

            if (!isFocused && !event.defaultPrevented) {
              navigation.navigate(route.name);
            }
          };


          return (
            <Pressable
              key={label}
              accessibilityRole="button"
              accessibilityState={isFocused ? {selected: true} : {}}
              accessibilityLabel={options.tabBarAccessibilityLabel}
              testID={options.tabBarTestID}
              onPress={onPress}
              style={{
                flex: 1,
                padding: 10,
                alignItems: 'center',
                justifyContent: 'center',
              }}>
              <View>
                <View style={{alignItems: 'center', justifyContent: 'center'}}>
                  {index === 0 ? (
                    <Image
                      style={{width: 20, height: 20}}
                      source={require(image1)}
                    />
                  ) : index === 1 ? (
                    <Image
                      style={{width: 20, height: 20}}
                      source={require(image2)}
                    />
                  ) : index === 2 ? (
                    <Image
                      style={{width: 20, height: 20}}
                      source={require(image3)}
                    />
                  ) : (
                    <Image
                      style={{width: 20, height: 20}}
                      source={require(image4)}
                    />
                  )}
                </View>
                <View>
                  <Text
                    style={{
                      fontFamily: variables.fonts.regularfont,
                      color: variables.colors.brandcolor,
                    }}>
                    {label}
                  </Text>
                </View>
              </View>
            </Pressable>
          );
        })}
      </View>
    </LinearGradient>
  );
}

export default function BottomTabNavigator() {
  return (
    <Tab.Navigator tabBar={(props) => <MyTabBar {...props} />}>
      <Tab.Screen
        name="home"
        options={{tabBarLabel: 'Home'}}
        component={Home}
      />
      <Tab.Screen
        name="search"
        options={{tabBarLabel: 'Search'}}
        component={Search}
      />
      <Tab.Screen
        name="cart"
        options={{tabBarLabel: 'Cart'}}
        component={Cart}
      />
      <Tab.Screen
        name="user"
        options={{tabBarLabel: 'Profile'}}
        component={StackNavigator}
      />
    </Tab.Navigator>
  );
}
