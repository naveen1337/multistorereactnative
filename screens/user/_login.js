import React, {useState} from 'react';
import {View, Text, StyleSheet, ScrollView} from 'react-native';
import {Image, Input, Button} from 'react-native-elements';
import variables from '../../variables';
import {useDispatch} from 'react-redux';
import messaging from '@react-native-firebase/messaging';

import {adddata} from '../../store/state/otp';

export default function login({navigation}) {
  const [phoneno, setphoneno] = useState('+911234567890');
  const [loading, setloading] = useState(false);
  const dispatch = useDispatch();

  async function initlogin() {
    try {
      setloading(true);
      console.log('LOGINNN >....');
      const token = await messaging().getToken();
      console.log(token);
      fetch('https://sevagan.co.in/dashboard/API/mobile_number_check.php', {
        method: 'POST',
        headers: {
          Accept: 'application/json',
          'Content-Type': 'application/json',
        },
        body: JSON.stringify({
          mobile_number: phoneno,
          fcm_token: token,
        }),
      }).then((data) => {
        data.json().then(async (parsed) => {
          console.log(parsed);
          if (parsed.status === 'success') {
            setloading(false);
            navigation.navigate('enterotp', {phoneno: phoneno});
          } else {
            throw new Error('ERRRROROORORR');
          }
        });
      });
    } catch (err) {
      console.log('Faild', err);
    }
  }

  return (
    <ScrollView style={styles.container}>
      <View style={styles.imagecontainer}>
        <Image
          style={styles.brandlogo}
          source={require('../../assets/brand/roundbrand.png')}
        />
      </View>
      <View>
        <Text style={styles.signtext}>SIGN IN</Text>
      </View>
      <View style={styles.inputcontainer}>
        <Input
          keyboardType="phone-pad"
          style={styles.inputbox}
          placeholder="Mobile No"
        />
      </View>
      <View style={styles.button}>
        <Button
          loading={loading}
          type="clear"
          loadingProps={{
            size: 'small',
            color: '#0000ff',
          }}
          containerStyle={{
            backgroundColor: variables.colors.brandcolor,
            borderRadius: 8,
            paddingVertical: 8,
          }}
          titleStyle={{
            color: '#000000',
          }}
          onPress={initlogin}
          title="SIGN UP"
        />
      </View>
    </ScrollView>
  );
}

const styles = StyleSheet.create({
  container: {
    backgroundColor: '#000000',
    flex: 1,
    paddingHorizontal: 30,
  },
  imagecontainer: {
    flexDirection: 'row',
    justifyContent: 'center',
    marginTop: 82,
  },
  brandlogo: {
    width: 200,
    height: 200,
  },
  signtext: {
    color: variables.colors.normaltext,
    fontSize: variables.fonts.large,
    textAlign: 'center',
    marginTop: 34,
    fontFamily: variables.fonts.medium,
  },
  inputcontainer: {
    marginTop: 35,
    marginBottom: 8,
  },
  inputbox: {
    color: '#FFFFFF',
  },
  button: {
    marginBottom: 30,
  },
});
