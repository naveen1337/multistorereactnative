import React from 'react';
import {View, Text, StyleSheet} from 'react-native';
import variables from '../../variables';
import TopBar from '../basics/topbar';

export default function CouponBox(props) {
  return (
    <View style={styles.root}>
      <View style={styles.topwrapper}>
        <Text style={styles.title}>{props.title}</Text>
        <Text style={styles.actiontext}>Apply</Text>
      </View>
      <Text style={styles.info}>
        {props.info}
      </Text>
    </View>
  );
}

const styles = StyleSheet.create({
  root: {
    marginHorizontal: 20,
    backgroundColor: '#ffffff',
    borderRadius: 15,
    paddingHorizontal: 8,
    paddingTop:15,
    marginTop:10
  },
  topwrapper: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems:"center",
    flexDirection:"row",
    justifyContent:"flex-start"

  },
  title: {
    textAlign: 'center',
    justifyContent:"center",
    borderStyle: 'dashed',
    borderRadius: 3,
    borderWidth: 1,
    fontFamily:variables.fonts.semiboldfont,
    fontSize:16,
    textTransform:"uppercase",
    letterSpacing:1,
    flex:6,
  },
  actiontext: {
    flex:2,
    textAlign: 'center',
    paddingHorizontal: 30,
    fontFamily:variables.fonts.semiboldfont,
    fontSize:16,
    color:variables.colors.brandcolor,
    textTransform:"uppercase",
    letterSpacing:1

  },
  info: {
    width: '100%',
    fontFamily:variables.fonts.regularfont,
    fontSize:16,
    paddingVertical:10,
  },
});
