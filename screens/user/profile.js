import React from 'react';
import {ScrollView, StyleSheet} from 'react-native';
import {Button} from 'react-native-elements';
import {TopBar, ContentBox} from '../../components';

import variables from '../../variables';

export default function Profile() {
  return (
    <ScrollView style={styles.root}>
      <TopBar varient="screen" title=" " />
      <ContentBox
        varient="usernamecard"
        name="The Example Name"
        email="example@example.com"
        phone="+91987654321"
      />
      <ContentBox varient="walletcard" amount="0" />
      <ContentBox varient="infocard" title="Order Status" />
      <ContentBox varient="infocard" title="Order History" />

      <ContentBox
        varient="addresscard"
        title="Home"
        address={['No 2178', 'Kamban Nagar', 'Rajagopalapuram', 'Pudukkottai']}
      />
      <ContentBox
        varient="addresscard"
        title="Office"
        address={['No 2178', 'Kamban Nagar', 'Rajagopalapuram', 'Pudukkottai']}
      />
      <Button
        type="clear"
        containerStyle={{
          backgroundColor: variables.colors.Primary,
          borderRadius: 8,
          paddingVertical: 8,
          margin: 10,
        }}
        titleStyle={{
          color: variables.colors.brandcolor,
        }}
        title="+ ADD ADDRESS"
      />
    </ScrollView>
  );
}

const styles = StyleSheet.create({
  root: {
    backgroundColor: variables.colors.brandbackground,
  },
});
