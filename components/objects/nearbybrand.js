import React from 'react';
import {View, Text, StyleSheet, Dimensions} from 'react-native';
import {Image} from 'react-native-elements';
import variables from '../../variables';
import Icon from 'react-native-vector-icons/Ionicons';

const NearByBrand = (props) => {
  return (
    <View style={styles.root}>
      <Image
        style={styles.image}
        source={require('../../assets/brand/5.png')}></Image>
      <View style={styles.inforoot}>
        <Text style={styles.brandtext} numberOfLines={1}>
          {props.brandname}
        </Text>
        <Text style={styles.categorytext}>category</Text>
        <View style={styles.textbottom}>
          <Text style={styles.infotextbottom}>Location</Text>
          <Icon name="ellipse" size={8} color={variables.colors.brandcolor} />
          <Text style={styles.infotextbottom}>Distance</Text>
        </View>
      </View>
    </View>
  );
};
const styles = StyleSheet.create({
  root: {
    width: Dimensions.get('window').width / 1.3,
    marginHorizontal: 15,
    marginVertical: 10,
    flexDirection: 'row',
  },
  image: {width: 100, height: 100},
  inforoot: {
    backgroundColor: '#ffffff',
    paddingHorizontal: 10,
  },

  brandtext: {
    fontFamily: variables.fonts.semiboldfont,
    fontSize: 18,
    width: Dimensions.get('window').width / 1.3 - 100,
  },
  categorytext: {
    fontFamily: variables.fonts.regularfont,
    fontSize: 12,
    color: variables.colors.brandcolor,
  },
  textbottom: {
    position: 'absolute',
    bottom: 0,
    flexDirection: 'row',
    width: '100%',
    justifyContent: 'space-between',
    paddingHorizontal: 10,
    alignItems:"center"
  },
  infotextbottom:{
    fontFamily: variables.fonts.regularfont,
    fontSize: 16,
  },
  brandinfo: {
    fontFamily: variables.fonts.regularfont,
    fontSize: 16,
  },
  dot: {
    fontSize: 10,
    color: variables.fonts.brandcolor,
  },
});
export default NearByBrand;
