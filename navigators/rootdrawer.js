import React from 'react';
import {
  View,
  Text,
  ScrollView,
  Pressable,
  Alert,
  StyleSheet,
} from 'react-native';
import Icon from 'react-native-vector-icons/Ionicons';
import {createDrawerNavigator} from '@react-navigation/drawer';
import variables from '../variables';

import {ContentBox} from '../components';

const Drawer = createDrawerNavigator();

import BottomTabNavigator from './bottomtabnav';

function CustomDrawerContent(props) {
  return (
    <View style={styles.root}>
      <ScrollView>
        <View>
          <View style={styles.header}>
            <Text style={styles.welcomtext}>Welcome !</Text>
            <Text style={styles.brandtext}>Tribata</Text>
          </View>

          <View style={styles.itemwrapper}>
            <ContentBox varient="walletcard" amount="100" />
          </View>
          <View style={styles.itemwrapper}>
            <ContentBox varient="infocard" title="Order Status" />
          </View>
          <View style={styles.itemwrapper}>
            <ContentBox varient="infocard" title="Order History" />
          </View>

          <View style={styles.itemtextwrapper}>
            <Text style={styles.itemtext}>About us</Text>
          </View>
          <View style={styles.itemtextwrapper}>
            <Text style={styles.itemtext}>Contact us</Text>
          </View>
        </View>
      </ScrollView>
      <View style={styles.actionwrapper}>
        <View style={styles.itemtextwrapper}>
          <Icon
            style={styles.icon}
            name="log-out-outline"
            size={20}
            color="#e84118"
          />
          <Text style={styles.actiontext}>Login/ Signup</Text>
        </View>
      </View>
    </View>
  );
}

export default function DrawerNav() {
  return (
    <Drawer.Navigator
      drawerContent={(props) => <CustomDrawerContent {...props} />}>
      <Drawer.Screen name="homescreen" component={BottomTabNavigator} />
    </Drawer.Navigator>
  );
}

const styles = StyleSheet.create({
  root: {backgroundColor: variables.colors.brandbackground, flex: 1},
  header: {
    height: 150,
    backgroundColor: '#000000',
    justifyContent: 'flex-end',
    alignItems: 'center',
    padding: 30,
  },
  welcomtext: {
    fontFamily: variables.fonts.regularfont,
    fontSize: 16,
    color: variables.colors.brandcolor,
  },
  brandtext: {
    fontFamily: variables.fonts.mediumfont,
    color: '#ffffff',
    paddingTop: 10,
    fontSize: 18,
  },
  itemwrapper: {
    flexDirection: 'row',
    alignItems: 'center',
  },
  itemtextwrapper: {
    flexDirection: 'row',
    padding: 15,
  },
  itemtext: {
    fontFamily: variables.fonts.regularfont,
    fontSize: 14,
  },
  icon: {paddingHorizontal: 20},
  actionwrapper: {
    position: 'absolute',
    width: '100%',
    bottom: 0,
    backgroundColor: variables.colors.brandbackground,
  },
  actiontext: {
    fontFamily: variables.fonts.semiboldfont,
    fontSize: 14,
    color: '#E11822',
  },
});
