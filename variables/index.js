exports.fonts = {
  regularfont: 'Poppins-Regular',
  mediumfont: 'Poppins-Medium',
  semiboldfont: 'Poppins-SemiBold',
  boldfont: 'Poppins-Bold',
  small: 12,
  regular: 16,
  large: 22,
};

exports.colors = {
  Primary: '#000000',
  normaltext: '#FEFEFE',
  brandcolor: '#CBA960',
  brandbackground: '#FFF8E8',
};
