import {createSlice} from '@reduxjs/toolkit';

const initialState = {data: []};
const otpSlice = createSlice({
  name: 'otp',
  initialState,
  reducers: {
    adddata(state, action) {
      state.data = action.payload;
    },
  },
});

export const {adddata} = otpSlice.actions;
export default otpSlice.reducer;
